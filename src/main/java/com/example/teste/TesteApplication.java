package com.example.teste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TesteApplication {
    @RequestMapping("/")
	public String home(){
		return "Lucas Coelho de Faria";
	}

	public static void main(String[] args) {
		SpringApplication.run(TesteApplication.class, args);
	}

}
